import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { AngularEditorModule } from '@kolkov/angular-editor';
import {VideoAnnotationComponent} from './video-annotation/video-annotation.component';

const routes: Routes = [
  {path: '', component: VideoAnnotationComponent},
];

@NgModule({
  imports: [
    HttpClientModule,
    RouterModule.forRoot(routes),
    AngularEditorModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [RouterModule, AngularEditorModule]
})
export class AppRoutingModule { }
