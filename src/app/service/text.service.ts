import { Injectable } from '@angular/core';
declare var d3: any;

@Injectable({
  providedIn: 'root'
})
export class TextService {
  constructor() { }
  // demo
  text(colorCode) {
    const innerWindow = document.getElementById('angle-canvas').getBoundingClientRect();
    const w = innerWindow.width;
    const h = innerWindow.height;
    const svg = d3.select('svg')
      .attr('width', w)
      .attr('height', h);
    const drag = d3.drag()
      .subject(Object)
      .on('drag', dragMove)
      .on('end', dragend);
    const newGroup = svg.append('g')
      .attr('class', 'editableText');
    const textPlaceholder = [{
      title: 'Enter Text Here',
    }];
    newGroup.append('rect')
      .attr('width', (w / 3.8))
      .attr('height', (h / 11))
      .attr('fill', colorCode)
      .attr('cursor', 'move')
      .attr('fill-opacity', .1)
      .call(drag);
    newGroup.selectAll('text')
      .data(textPlaceholder)
      .enter()
      .append('foreignObject')
      .attr('width', (w / 4))
      .attr('height', (h / 14))
      .style('border', '2px dashed' + colorCode)
      .append('xhtml:div')
      .style('color', colorCode)
      .style('font-family', '"Lato", sans-serif')
      .style('font-size', 22)
      .style('font-weight', 600)
      .style('padding', '10px')
      .style('letter-spacing', '1px')
      .style('display', 'flex')
      .style('justify-content', 'center')
      .style('align-item', 'center')
      .style('text-item', 'center')
      .style('width', '100%')
      .attr('contentEditable', true)
      // tslint:disable-next-line:only-arrow-functions
      .text(d => {
        return d.title;
      })
      .on('click', dragMove);
    function  dragMove() {
        d3.select(this)
          .attr('x', d3.event.x)
          .attr('y', d3.event.y);
        d3.select(this.nextSibling)
          .attr('x', d3.event.x + 10)
          .attr('y', (d3.event.y + 10));
        d3.select(this.parentNode).attr('id', 'deleting');
        if ( d3.event.x  <= 30) {
         d3.selectAll('#deleting').remove();
       }
    }

    function  dragend() {
      d3.select(this.parentNode).attr('id', '');
    }
  }
}
