import { TestBed } from '@angular/core/testing';

import { SpeedImgUploadService } from './speed-img-upload.service';

describe('SpeedImgUploadService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SpeedImgUploadService = TestBed.get(SpeedImgUploadService);
    expect(service).toBeTruthy();
  });
});
