import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class VideoService {
  apiURL = environment.apiUrl;
  tempToken = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYXBpLWRldi1maXMuYXBwcWlrLnVzXC9hcGlcL2xvZ2luIiwiaWF0IjoxNjExNTU2NDcyLCJleHAiOjE2MTIxNjEyNzIsIm5iZiI6MTYxMTU1NjQ3MiwianRpIjoiYWt3ODhmY3NCR3ZIb1QyTiIsInN1YiI6MSwicHJ2IjoiODdlMGFmMWVmOWZkMTU4MTJmZGVjOTcxNTNhMTRlMGIwNDc1NDZhYSIsImRhdGEiOnsiaHR0cF91c2VyX2FnZW50IjoiUG9zdG1hblJ1bnRpbWVcLzcuMjYuOCIsInNlcnZlcl9wcm90b2NvbCI6IkhUVFBcLzEuMSIsInJlbW90ZV9hZGRyIjoiMTEyLjE5Ni4xOTEuNjUiLCJ1c2VyX2lkIjoxLCJyb2xlIjoiYWRtaW4iLCJ1c2VyX2RldGFpbCI6eyJ1c2VyX2FjY291bnRfaWQiOjEsImZpcnN0X25hbWUiOiJGSVMiLCJsYXN0X25hbWUiOiJBRE1JTiIsImxvZ2luX2VtYWlsIjoiYWFwcHFpa0BnbWFpbC5jb20ifSwidGltZSI6IjIwMjEtMDEtMjVUMDY6MzQ6MzIuMDU1NTM0WiIsImV4cGlyZV9pbiI6MTAwODB9fQ.MsvO7moPZ04gQDWmZa9yQkzw_pIi3ghq1_uLElaE9yY'
  constructor(
      private http: HttpClient,
  ) {
  }


  get(route: string, params: HttpParams | { [param: string]: string | string[] } = new HttpParams()): Observable<any> {
    const header = new HttpHeaders();
    // const tempToken = localStorage.getItem('token');
    const other_header = header.append('Authorization', 'Bearer ' + this.tempToken);
    return this.http.get<any>(this.apiURL + route, {headers: other_header});
  }
  post(route: string, body: any): Observable<any> {
    const header = new HttpHeaders();
    // const tempToken = localStorage.getItem('token');
    const other_header = header.append('Authorization', 'Bearer ' + this.tempToken);
    return this.http.post<any>(this.apiURL + route, body, {headers: other_header});
  }
  /*postRaw(path: string, body: any): Observable<any> {
    return this.http.post(
        `${this.apiURL}${path}`,
        body
    );
  }*/
}

