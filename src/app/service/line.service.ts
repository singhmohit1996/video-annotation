import { Injectable } from '@angular/core';
declare var d3: any;

@Injectable({
  providedIn: 'root'
})
export class LineService {
  constructor() { }
  lines(colorCode, stopDrag: boolean) {
    // @ts-ignore
    const innerWindow = document.getElementById('angle-canvas').getBoundingClientRect();
    const w = innerWindow.width;
    const h = innerWindow.height;
    let svg;
    let activeLine = null;
    const renderPath = d3.line()
      .curve(d3.curveCardinal.tension(2))
      .x( d => d[0])
      .y( d => d[1]);
    if (stopDrag) {
      svg = d3.select('svg')
        .call(d3.drag()
          .on('drag', null));
    } else {
     svg = d3.select('svg')
       .attr('width', w)
       .attr('height', h)
        .call(d3.drag()
          .on('start', dragstarted)
          .on('drag', dragged)
          .on('end', dragended));
    }
    function dragstarted() {
      activeLine = svg.append('path').datum([]).attr('class', 'line').attr('stroke', colorCode);
      activeLine.datum().push(d3.mouse(this));
    }

    function dragged() {
      activeLine.datum().push(d3.mouse(this));
      activeLine.attr('d', renderPath);
    }

    function dragended() {
      activeLine = null;
    }
  }
}
