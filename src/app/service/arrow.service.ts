import { Injectable } from '@angular/core';
declare var d3: any;

@Injectable({
  providedIn: 'root'
})
export class ArrowService {
  constructor() { }
  // demo
  arrows(colorCode) {
    const w = window.innerWidth * 0.9;
    const h = window.innerHeight;
    const svg = d3.select('svg')
      .attr('width', w)
      .attr('height', h);
    const newGroup = svg.append('g')
      .attr('class', 'lines');
    const drag = d3.drag()
      .subject(Object)
      .on('drag', dragMove)
      .on('end', dragend);
    const dragHandel = d3.drag()
      .subject(Object)
      .on('drag', dragHandles)
      .on('end', dragend);
    /*const rotateLines = d3.drag()
      .subject(Object)
      .on('drag', rotateLine);*/
    newGroup.append('circle')
      .attr('cx', 110)
      .attr('cy', 107)
      .attr('r', 5)
      .attr('class', 'leftHandle')
      .attr('stroke', colorCode)
      .attr('stroke-width', '4')
      .attr('fill', colorCode)
      .attr('cursor', 'crosshair')
      .call(dragHandel);
    const line = newGroup.append('rect')
      .attr('x', 105)
      .attr('y', 105)
      .attr('width', 250)
      .attr('height', 5)
      .attr('fill', colorCode)
      .attr('class', 'line')
      .attr('cursor', 'move')
      .call(drag);
    newGroup.append('polyline')
      .attr('transform', `translate(${350}, ${100})`)
      .attr('points', '14,7,0,0,0,14')
      .attr('fill', colorCode)
      .attr('class', 'rightArrow');

    function  dragMove(d) {
      line.attr('x',  d3.event.x)
        .attr('y',  d3.event.y);
      // tslint:disable-next-line:radix
      const rectangleWidth = parseInt(d3.select(this).attr('width'));
      // tslint:disable-next-line:radix
      const rectangleXAxis = parseInt(d3.select(this).attr('x'));
      // tslint:disable-next-line:radix
      const rectangleYAxis = parseInt(d3.select(this).attr('y'));
      d3.select(this.previousSibling)
        .attr('cx', (rectangleXAxis - 5))
        .attr('cy', (rectangleYAxis + 3));
      d3.select(this.nextSibling)
        .attr('transform', `translate(${rectangleXAxis + (rectangleWidth)}, ${rectangleYAxis - 4})`);
      d3.select(this.parentNode).attr('id', 'dragging_for_rotate');
      d3.select(this.parentNode).attr('id', 'deleting');
      d3.selectAll('#deleting .draghandle')
        .attr('cx', (d3.event.x + 50))
        .attr('cy',  d3.event.y);
      if ( d3.event.x  <= 30) {
        d3.selectAll('#deleting').remove();
      }
    }
    function  dragHandles() {
      d3.select(this.parentNode).attr('id', 'dragging');
      d3.select('#dragging .line').attr('width', (d3.event.x / 1.5));
      // tslint:disable-next-line:radix
      const rectangleWidth = parseInt(d3.select('#dragging .line').attr('width'));
      // tslint:disable-next-line:radix
      const rectangleXAxis = parseInt(d3.select('#dragging .line').attr('x'));
      // tslint:disable-next-line:radix
      const rectangleYAxis = parseInt(d3.select('#dragging .line').attr('y'));

      d3.select('#dragging .rightArrow')
      .attr('transform', `translate(${rectangleXAxis + (rectangleWidth)}, ${rectangleYAxis - 4})`);
    }
    function  dragend() {
      d3.select(this.parentNode).attr('id', '');
    }
  }
  // line service.ts code
  /*lines(colorCode) {
    const w = window.innerWidth * 0.9;
    const h = window.innerHeight;
    const svg = d3.select('svg')
      .attr('width', w)
      .attr('height', h);
    const newGroup = svg.append('g')
      .attr('class', 'lines');
    const drag = d3.drag()
      .subject(Object)
      .on('drag', dragMove)
      .on('end', dragend);
    const dragHandel = d3.drag()
      .subject(Object)
      .on('drag', dragHandles)
      .on('end', dragend)
      .container(function() { return this.parentNode.parentNode; });

    newGroup.append('circle')
      .attr('cx', 110)
      .attr('cy', 107)
      .attr('r', 5)
      .attr('class', 'leftHandle')
      .attr('stroke', colorCode)
      .attr('stroke-width', '4')
      .attr('fill', colorCode)
      .attr('cursor', 'crosshair')
      .call(dragHandel);
    const line = newGroup.append('rect')
      .attr('x', 105)
      .attr('y', 105)
      .attr('width', 250)
      .attr('height', 5)
      .attr('fill', colorCode)
      .attr('class', 'line')
      .attr('cursor', 'move')
      .call(drag);
    newGroup.append('circle')
      .attr('cx', 360)
      .attr('cy', 107)
      .attr('r', 5)
      .attr('class', 'rightHandle')
      .attr('stroke', colorCode)
      .attr('stroke-width', '4')
      .attr('fill', colorCode)
      .attr('cursor', 'crosshair')
      .call(dragHandel);
    newGroup.append('rect')
      .attr('x', 245)
      .attr('y', 125)
      .attr('width', 10)
      .attr('height', 10)
      .attr('fill', colorCode)
      .attr('class', 'rotating_handle')
      .attr('cursor', 'cell');
    function  dragMove() {
      line
        .attr('x',  d3.event.x)
        .attr('y',  d3.event.y);
      // tslint:disable-next-line:radix
      const rectangleWidth = parseInt(d3.select(this).attr('width'));
      // tslint:disable-next-line:radix
      const rectangleXAxis = parseInt(d3.select(this).attr('x'));
      // tslint:disable-next-line:radix
      const rectangleYAxis = parseInt(d3.select(this).attr('y'));
      d3.select(this.previousSibling)
        .attr('cx', (rectangleXAxis - 5))
        .attr('cy', (rectangleYAxis + 3));
      d3.select(this.nextSibling)
        .attr('cx', (rectangleXAxis + (rectangleWidth + 5)))
        .attr('cy', (rectangleYAxis + 3));
      d3.select(this.parentNode).attr('id', 'deleting');
      d3.select('#deleting .rotating_handle')
        .attr('x', ((rectangleWidth / 2) + rectangleXAxis))
        .attr('y',  d3.event.y + 20);
      d3.selectAll('#deleting .draghandle')
        .attr('cx', (d3.event.x + 50))
        .attr('cy',  d3.event.y);
      if ( d3.event.x  <= 30) {
        d3.selectAll('#deleting').remove();
      }
    }
    function  dragHandles() {
      d3.select(this.parentNode).attr('id', 'dragging');
      d3.select('#dragging .line').attr('width', (d3.event.x / 1.5));
      // tslint:disable-next-line:radix
      const rectangleWidth = parseInt(d3.select('#dragging .line').attr('width'));
      // tslint:disable-next-line:radix
      const rectangleXAxis = parseInt(d3.select('#dragging .line').attr('x'));
      // tslint:disable-next-line:radix
      const rectangleYAxis = parseInt(d3.select('#dragging .line').attr('y'));
      d3.select('#dragging .rotating_handle')
        .attr('x', ((rectangleWidth / 2) + rectangleXAxis))
        .attr('y',  d3.event.y + 20);
      d3.select('#dragging .rightHandle').attr('cx', (rectangleWidth + rectangleXAxis));
      d3.select('#dragging .rotate_element')
        .attr('x', (rectangleXAxis + (rectangleWidth / 2) ))
        .attr('y', (rectangleYAxis + 30));
    }
    function  dragend() {
      d3.select(this.parentNode).attr('id', '');
    }
  }*/
}
