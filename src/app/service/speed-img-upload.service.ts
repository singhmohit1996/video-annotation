import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import {VideoService} from "./video.service";
@Injectable({
  providedIn: 'root'
})
export class SpeedImgUploadService {

  constructor(
      private  apiService: VideoService,
  ) { }

  uploadImage(image,imageFor) {
    const formData = new FormData();
    formData.append('image', image);
    formData.append('image_for', imageFor);
    return this.apiService.post(`/upload/image`, formData)
        .pipe(
            tap(response => console.log('start..')),
            catchError(error => {
              return of(false);
            }),
            map(res => {
              return res;
            })
        );
  }

}

