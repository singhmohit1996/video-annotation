import { Injectable } from '@angular/core';
import Konva from 'konva';
@Injectable({
  providedIn: 'root'
})
export class ShapeService {
  constructor() { }
  circle(colorCode) {
    return new Konva.Circle({
      x: 100,
      y: 100,
      radius: 70,
      fill: '',
      stroke: colorCode,
      strokeWidth: 2,
      draggable: true
    });
  }
  line(colorCode) {
    return new Konva.Line({
      points: [5, 70, 140, 23],
      stroke: colorCode,
      strokeWidth: 3,
      lineCap: 'round',
      lineJoin: 'round',
      draggable: true
    });
  }
  rectangle(colorCode) {
    return new Konva.Rect({
      x: 20,
      y: 20,
      width: 100,
      height: 50,
      fill: '',
      stroke: colorCode,
      strokeWidth: 2,
      draggable: true
    });
  }

  arrow(colorCode) {
    return new Konva.Arrow({
      x: 10,
      y: 10,
      points: [0, 0, 50, 50],
      pointerLength: 20,
      pointerWidth: 10,
      fill: colorCode,
      stroke: colorCode,
      strokeWidth: 2,
      draggable: true
    });

    }
  angles(colorCode) {
  return new  Konva.Shape({
    x: 10,
    y: 20,
    fill: '#00D2FF',
    width: 100,
    height: 50,
    draggable: true,
    // tslint:disable-next-line:only-arrow-functions
    sceneFunc(context, shape) {
      context.beginPath();
      context.moveTo(120, 150);
      context.lineTo(220, 80);
      context.quadraticCurveTo(150, 100, 260, 170);
      context.closePath();
      // (!) Konva specific method, it is very important
      context.fillStrokeShape(shape);
    },
  });
  }
}
