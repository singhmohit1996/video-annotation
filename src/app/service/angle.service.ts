import { Injectable } from '@angular/core';
declare var d3: any;

@Injectable({
  providedIn: 'root'
})
export class AngleService {
  constructor() { }
  // demo
  angles(colorCode) {
    // tslint:disable-next- line:one-variable-per-declaration
    const innerWindow = document.getElementById('angle-canvas').getBoundingClientRect();
    const width = innerWindow.width;
    const height = innerWindow.height;
    const svg = d3.select('svg')
      .attr('width', width)
      .attr('height', height)
      .append('g')
      .attr('class', 'select')
      .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')');
    const drag = d3.drag()
      .on('drag', d => {
        d.x = d3.event.x;
        d.y = d3.event.y;
        update();
      });

    const arc = d3.arc()
      .cornerRadius(2);

    // Origin
    svg.append('line')
      .attr('y1', -height / 6)
      .attr('y2', height / 6);

    svg.append('line')
      .attr('x1', -width / 8)
      .attr('x2', width / 8);

    svg.append('circle')
      .attr('r', 4)
      .attr('stroke', colorCode);

    // Difference
    const differenceArc = svg.append('g')
      .datum({});

    const differencePath = differenceArc.append('path')
      .attr('class', 'difference');

    const differenceText = differenceArc.append('text');

    // Source vector
    const sourceVector = svg.append('g')
      .attr('class', 'source')
       .datum({x: 0, y: -150});
    const parentNodeInitialize = d3.drag()
      .on('drag' , onDragParentGroup)
      .on('end', onStopDragParentGroup);

    d3.selectAll('g.select').call(parentNodeInitialize);

    const sourceHandle = sourceVector.append('g')
      .attr('class', 'handle')
      .call(drag);

    const sourceLine = sourceVector.append('line').attr('stroke', colorCode);
    sourceHandle.append('circle')
      .attr('fill', colorCode)
      .attr('r', 10);

    const sourceText = sourceHandle.append('text')
      .attr('dy', -15);
    // Compare  vector
    const compareVector = svg.append('g')
      .attr('class', 'compare')
      .datum({x: -250, y: -75});

    const compareHandle = compareVector.append('g')
      .attr('class', 'handle')
      .call(drag);

    const compareLine = compareVector.append('line').attr('stroke', colorCode);

    compareHandle.append('circle')
      .attr('fill', colorCode)
      .attr('r', 10);

    const compareText = compareHandle.append('text')
      .attr('dy', -15);

    // Update
    function update() {
      const source = sourceVector.datum();
      const compare = compareVector.datum();

      const sourceLength = Math.sqrt(source.x * source.x + source.y * source.y);
      const compareLength = Math.sqrt(compare.x * compare.x + compare.y * compare.y);

      // The math-y bits
      const a2 = Math.atan2(source.y, source.x);
      const a1 = Math.atan2(compare.y, compare.x);
      const sign = a1 > a2 ? 1 : -1;
      let angle = a1 - a2;
      const K = -sign * Math.PI * 2;
      angle = (Math.abs(K + angle) < Math.abs(angle)) ? K + angle : angle;

      sourceLine
        .attr('x2', (d) => d.x)
        .attr('y2', (d) => d.y);

      sourceHandle
        .attr('transform', (d) => `translate(${d.x}, ${d.y})`);
      /*    sourceText
            .text(`${source.x}, ${source.y}`);*/
      compareLine
        .attr('x2', (d) => d.x)
        .attr('y2', (d) => d.y);

      compareHandle
        .attr('transform', (d) => `translate(${d.x}, ${d.y})`);
      /*    compareText
            .text(`${compare.x}, ${compare.y}`);*/
      arc
        .innerRadius(10)
        .outerRadius(Math.max(60, Math.min(sourceLength, compareLength) * 0.6))
        .startAngle(a2 + Math.PI / 2)
        .endAngle(a2 + angle + Math.PI / 2);

      differencePath
/*
        .style('fill', angle > 0 ? 'yellow' : '#4cd137')
*/
        .style('fill', colorCode)
        .attr('d', arc());

      differenceText
        .attr('transform', 'translate(' + 50 + ',' + 50 + ')')
        .text(
          (Math.abs(360 * angle / (Math.PI * 2))).toFixed(2) + 'º ')
        .attr('fill', colorCode);
    }
    function onDragParentGroup() {
        d3.select(this)
          .attr('id', 'parentnode')
          .attr('cursor', 'move')
          .attr('transform', 'translate(' + d3.event.x + ',' + d3.event.y + ')');
        if ( d3.event.x  <= 30) {
            d3.selectAll('#parentnode').remove();
          }
    }
    function onStopDragParentGroup() {
      d3.select(this)
        .attr('id', '')
        .attr('cursor', 'initial');
    }
    update();
  }
}
