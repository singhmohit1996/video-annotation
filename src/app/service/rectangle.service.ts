import { Injectable } from '@angular/core';
declare var d3: any;

@Injectable({
  providedIn: 'root'
})
export class RectangleService {
  constructor() { }
  // demo
  rectangles(colorCode) {
    const innerWindow = document.getElementById('angle-canvas').getBoundingClientRect();
    const w = innerWindow.width;
    const h = innerWindow.height;
    const isXChecked = true;
    const  isYChecked = true;
    let width = 200;
    let height = 150;
    const  dragAngle = 20;

    const drag = d3.drag()
      .subject(Object)
      .on('drag', dragMove)
      .on('end', dragend);

    const dragRight = d3.drag()
      .subject(Object)
      .on('drag', rDragResize);

    const dragLeft = d3.drag()
      .subject(Object)
      .on('drag', lDragResize);

    const dragtop = d3.drag()
      .subject(Object)
      .on('drag', tdragresize);

    const dragbottom = d3.drag()
      .subject(Object)
      .on('drag', bdragresize);

    const svg = d3.select('svg')
      .attr('width', w)
      .attr('height', h);

    const newg = svg.append('g')
      .attr('class', 'rectangle')
      .data([{x: width / 2, y: height / 2}]);


    const dragrect = newg.append('rect')
      .attr('id', 'active')
      .attr('x', d => d.x)
      .attr('y', d => d.y)
      .attr('height', height)
      .attr('width', width)
      .attr('fill', colorCode)
      .attr('fill-opacity', 0.3)
      .attr('cursor', 'move')
      .call(drag);

    const dragbarleft = newg.append('rect')
          .attr('x', d => d.x - (dragAngle / 2))
            // tslint:disable-next-line:no-shadowed-variable
          .attr('y', d => d.y + (dragAngle / 2))
          .attr('height', height - dragAngle)
          .attr('id', 'dragLeft')
          .attr('width', (dragAngle / 2))
          .attr('fill', colorCode)
          .attr('fill-opacity', 1)
          .attr('cursor', 'ew-resize')
            .call(dragLeft);

    const dragBarRight = newg.append('rect')
      .attr('x', d => d.x + width - (dragAngle / 2))
      .attr('y', d => d.y + (dragAngle / 2))
      .attr('id', 'dragRight')
      .attr('height', height - dragAngle)
      .attr('width', (dragAngle / 2))
      .attr('fill', colorCode)
      .attr('fill-opacity', 1)
      .attr('cursor', 'ew-resize')
      .call(dragRight);

    const dragBarTop = newg.append('rect')
      .attr('x', d => d.x + (dragAngle / 2))
      .attr('y', d => d.y - (dragAngle / 2))
      .attr('height', (dragAngle / 2))
      .attr('id', 'dragLeft')
      .attr('width', width - dragAngle)
      .attr('fill', colorCode)
      .attr('fill-opacity', 1)
      .attr('cursor', 'ns-resize')
      .call(dragtop);

    const dragBarBottom = newg.append('rect')
      .attr('x', d => d.x + (dragAngle / 2))
      .attr('y', d => d.y + height - (dragAngle - 20 ))
      .attr('id', 'dragRight')
      .attr('height', (dragAngle / 2))
      .attr('width', width - dragAngle)
      .attr('fill', colorCode)
      .attr('fill-opacity', 1)
      .attr('cursor', 'ns-resize')
      .call(dragbottom);

    function dragMove(d) {
      if (isXChecked) {
        dragrect
          .attr('x', d.x = Math.max(0, Math.min(w - width, d3.event.x)));
        dragbarleft
          // tslint:disable-next-line:no-shadowed-variable
          .attr('x', d => d.x - (dragAngle / 2));
        dragBarRight
          // tslint:disable-next-line:no-shadowed-variable
          .attr('x', d => d.x + width - (dragAngle / 2));
        dragBarTop
          // tslint:disable-next-line:no-shadowed-variable
          .attr('x', d =>  d.x + (dragAngle / 2));
        dragBarBottom
          // tslint:disable-next-line:no-shadowed-variable
          .attr('x', d =>  d.x + (dragAngle / 2));
      }
      if (isYChecked) {
        dragrect
          .attr('y', d.y = Math.max(0, Math.min(h - height, d3.event.y)));
        dragbarleft
          // tslint:disable-next-line:no-shadowed-variable
          .attr('y',  d => d.y + (dragAngle / 2));
        dragBarRight
          // tslint:disable-next-line:no-shadowed-variable
          .attr('y',  d => d.y + (dragAngle / 2));
        dragBarTop
          // tslint:disable-next-line:no-shadowed-variable
          .attr('y',  d => d.y - (dragAngle / 2));
        dragBarBottom
          // tslint:disable-next-line:no-shadowed-variable
          .attr('y',  d => d.y + height - (dragAngle / 2));
      }
      d3.select(this.parentNode).attr('id', 'deleting');
      if (d.x === 0) {
        console.log('is deleting');
        d3.selectAll('#deleting').remove();
      }
    }
    function  dragend() {
      d3.select(this.parentNode).attr('id', '');
    }

    function lDragResize(d) {
      if (isXChecked) {
        const oldX = d.x;
        d.x = Math.max(0, Math.min(d.x + width - (dragAngle / 2), d3.event.x));
        width = width + (oldX - d.x);
        dragbarleft
          // tslint:disable-next-line:no-shadowed-variable
          .attr('x', d => d.x - (dragAngle / 2));
        dragrect
          // tslint:disable-next-line:no-shadowed-variable
          .attr('x', d =>  d.x)
          .attr('width', width);

        dragBarTop
          // tslint:disable-next-line:no-shadowed-variable
          .attr('x', d => d.x + (dragAngle / 2))
          .attr('width', width - dragAngle);
        dragBarBottom
          // tslint:disable-next-line:no-shadowed-variable
          .attr('x', d =>  d.x + (dragAngle / 2))
          .attr('width', width - dragAngle);
      }
    }

    function rDragResize(d) {
      if (isXChecked) {
        const dragX = Math.max(d.x + (dragAngle / 2), Math.min(w, d.x + width + d3.event.dx));
        width = dragX - d.x;
        dragBarRight
          // tslint:disable-next-line:no-shadowed-variable
          .attr('x', d => dragX - (dragAngle / 2));
        dragrect
          .attr('width', width);
        dragBarTop
          .attr('width', width - dragAngle);
        dragBarBottom
          .attr('width', width - dragAngle);
      }
    }

    function tdragresize(d) {

      if (isYChecked) {
        const oldY = d.y;
        // Max x on the right is x + width - dragAngle
        // Max x on the left is 0 - (dragAngle/2)
        d.y = Math.max(0, Math.min(d.y + height - (dragAngle / 2), d3.event.y));
        height = height + (oldY - d.y);
        dragBarTop
          // tslint:disable-next-line:no-shadowed-variable
          .attr('y', d => d.y - (dragAngle / 2));

        dragrect
          // tslint:disable-next-line:no-shadowed-variable
          .attr('y', d => d.y)
          .attr('height', height);

        dragbarleft
          // tslint:disable-next-line:no-shadowed-variable
          .attr('y', d => d.y + (dragAngle / 2))
          .attr('height', height - dragAngle);
        dragBarRight
          // tslint:disable-next-line:no-shadowed-variable
          .attr('y', d => d.y + (dragAngle / 2))
          .attr('height', height - dragAngle);
      }
    }

    function bdragresize(d) {
      if (isYChecked) {
        const dragy = Math.max(d.y + (dragAngle / 2), Math.min(h, d.y + height + d3.event.dy));

        // recalculate width
        height = dragy - d.y;

        // move the right drag handle
        dragBarBottom
          // tslint:disable-next-line:no-shadowed-variable
          .attr('y', d => dragy - (dragAngle / 2));
        dragrect
          .attr('height', height);
        dragbarleft
          .attr('height', height - dragAngle);
        dragBarRight
          .attr('height', height - dragAngle);
      }
    }

  }
}
