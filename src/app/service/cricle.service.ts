import { Injectable } from '@angular/core';
declare var d3: any;

@Injectable({
  providedIn: 'root'
})
export class CircleService {
  constructor() { }
  // demo
  circles(colorCode) {
    const innerWindow = document.getElementById('angle-canvas').getBoundingClientRect();
    const w = innerWindow.width;
    const h = innerWindow.height;
    const drag = d3.drag()
      .subject(Object)
      .on('drag', dragMove)
      .on('end', dragend);

    const dragHandles = d3.drag()
      .subject(Object)
      .on('drag', dragLeftRight);

    const svg = d3.select('svg')
      .attr('width', w)
      .attr('height', h);
    const newGroup = svg.append('g')
      .attr('class', 'circle');
    const circles = newGroup.append('circle')
      .attr('cx', 100)
      .attr('cy', 100)
      .attr('r', 50)
      .attr('class', 'maincricle')
      .attr('stroke', colorCode)
      .attr('stroke-width', '4')
      .attr('cursor', 'move')
      .attr('fill', colorCode)
      .attr('fill-opacity', '0.1')
      .call(drag);

    newGroup.append('circle')
      .attr('cx', (100 + 50))
      .attr('cy', 100)
      .attr('r', 5)
      .attr('fill', colorCode)
      .attr('fill-opacity', '1')
      .attr('class', 'draghandle')
      .attr('cursor', 'ew-resize')
      .call(dragHandles);

    function  dragMove(d) {
      circles.attr('cx',  d3.event.x)
        .attr('cy',  d3.event.y);
      d3.select(this.parentNode).attr('id', 'deleting');
      d3.selectAll('#deleting .draghandle')
        .attr('cx', (d3.event.x + 50))
        .attr('cy',  d3.event.y);
      if ( d3.event.x  <= 30) {
        d3.selectAll('#deleting').remove();
      }
      // tslint:disable-next-line:radix
      const bigCircleXAxis = parseInt(d3.select(this).attr('cx'));
      // tslint:disable-next-line:radix
      const bigCircleRadius = parseInt(d3.select(this).attr('r'));
      d3.select(this.nextSibling)
        .attr('cx' , ( bigCircleXAxis + bigCircleRadius));
      d3.select(this.previousSibling).attr('r' ,  (d3.event.x - 100));
    }
    function  dragend() {
      d3.select(this.parentNode).attr('id', '');
    }
    function  dragLeftRight() {
       // tslint:disable-next-line:radix
      const bigCircleXAxis = parseInt(d3.select(this.previousSibling).attr('cx'));
      // tslint:disable-next-line:radix
      const bigCircleRadius = parseInt(d3.select(this.previousSibling).attr('r'));
      d3.select(this)
        .attr('cursor', 'ew-resize')
        .attr('cx' , ( bigCircleXAxis + bigCircleRadius));
      d3.select(this.previousSibling).attr('r' ,  (d3.event.x - 100));
    }

  }
}
