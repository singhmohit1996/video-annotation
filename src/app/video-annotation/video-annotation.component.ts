import {Component, ElementRef, HostListener, Inject, Input, OnInit, ViewChild} from '@angular/core';
import {VgAPI} from 'videogular2/compiled/core';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DOCUMENT} from '@angular/common';
import {AngularEditorConfig} from '@kolkov/angular-editor';
import {AngleService} from '../service/angle.service';
import {RectangleService} from '../service/rectangle.service';
import {CircleService} from '../service/cricle.service';
import {LineService} from '../service/line.service';
import {ArrowService} from '../service/arrow.service';
import {TextService} from '../service/text.service';
import {DomSanitizer} from '@angular/platform-browser';
import html2canvas from 'html2canvas';
import {VideoService} from "../service/video.service";
import {SpeedImgUploadService} from "../service/speed-img-upload.service";
declare var d3: any;
@Component({
  selector: 'app-video-annotation',
  templateUrl: './video-annotation.component.html',
  styleUrls: ['./video-annotation.component.css']
})
export class VideoAnnotationComponent implements OnInit {
  videoAnnotateImg: any;
  constructor(
    @Inject(DOCUMENT) private document: Document,
    private _FB: FormBuilder,
    private sanitizer: DomSanitizer,
    private rectangleService: RectangleService,
    private textService: TextService,
    private angleService: AngleService,
    private  circleService: CircleService,
    private lineService: LineService,
    private arrowService: ArrowService,
    private videoServices: VideoService,
    private speedImgUploadService: SpeedImgUploadService
    ) { }
  videoAnnotateData = [];
  commentForm: FormGroup;
  selectedColorCode = '#ee5253';
  isSettingTabOpen: boolean;
  videoTabContent: Array<any>;
  colors: Array<any>;
  hasWindowToggle: boolean;
  tabName = 'descriptions';
  noteForm: FormGroup;
  api: VgAPI;
  currentPauseTime: string;
  userImageType: string;
  capturedImgSrc: string;
  public imageTypes = ['JPG', 'PNG', 'BMP', 'TIFF', 'GIF', 'PPM', 'PGM'];
  videoTimeSlots = [];
  isToggleNotesForm: boolean;
  isCLearAllEnabled = false;
  videoData: any;
  @ViewChild('myMedia', {static: false }) public  videoElement: ElementRef;
  @ViewChild('videoWrapper', {static: false }) public  videoWrapper: ElementRef;
  @ViewChild('video_canvas', {static: false }) public  videoCanvas: HTMLCanvasElement;
  @ViewChild('screen', {static: false }) screen: ElementRef;
  // tslint:disable-next-line:no-input-rename
  @Input('snapshotName') public snapshotName: string;
  // Konva configurations
  shapes: any = [];
  selectedButton: any = {
    circle: false,
    rectangle: false,
    line: false,
    undo: false,
    erase: false,
    text: false
  };
  // konva functions start
  isVideoFullScreen = false;

  // fixed right sidebar on scroll
  @HostListener('window:scroll', [])
  onWindowScroll() {
    if (document.body.scrollTop > 20 ||
      document.documentElement.scrollTop > 20) {
      document.getElementById('fi_comment_sidebar').classList.add('sticky');
    } else {
      document.getElementById('fi_comment_sidebar').classList.remove('sticky');
    }
  }

  ngOnInit() {
    this.getSpeedData();
    // video tab array
    this.videoTabContent = [
      { id: '1', title: 'descriptions' },
      { id: '2', title: 'notes' },
      { id: '3', title: 'Account tags' },
      { id: '4', title: 'Group tags' },
    ];
    this.colors = [
      { id: '1', colorCode: '#27ae60' },
      { id: '2', colorCode: '#c0392b' },
      { id: '3', colorCode: '#fff' },
      { id: '4', colorCode: '#f1c40f' },
      { id: '5', colorCode: '#26de81' },
      { id: '6', colorCode: '#2bcbba' },
      { id: '7', colorCode: '#fc5c65' },
      { id: '8', colorCode: '#2bcbba' },
      { id: '9', colorCode: '#45aaf2' },
      { id: '10', colorCode: '#fffa65' },
      { id: '11', colorCode: '#32ff7e' },
      { id: '12', colorCode: '#32ff7e' }
    ];
    this.noteFormInit();
    this.commentFormInit();
  }

  // note form initialization
  noteFormInit() {
    this.noteForm = this._FB.group({
      video_id: [''],
      annotations : this._FB.array([])
    });
  }
  commentFormInit() {
    this.commentForm = this._FB.group({
      description: ['', Validators.required],
      label: ['', Validators.required]
    })
  }

  get AnnotationsArray(){
    return <FormArray>this.noteForm.get('annotations');
  }
  // After initialized player you can listen events dispatched by video
  onPlayerReady(api: VgAPI) {
    this.api = api;
  }

  // video detail tabs
  showTab(tabName) {
    this.getSnapShotAtCurrentTime();
    this.isVideoFullScreen = false;
    this.tabName = tabName;
    if (this.tabName === 'notes') {
      this.api.pause();
      this.isToggleNotesForm = false;
      if (this.api.pause) {
        // get scrubBar aria-valuenow data
        const scrubBar = document.getElementsByClassName('scrubBar') as HTMLCollection;
        const scrubBarAriaValue = scrubBar[0].getAttribute('aria-valuenow').replace('%', '');
        // converting video current time for UI purpose
        this.currentPauseTime = this.timeConverter(this.api.currentTime);
        /*capture screen shot */
      }
    }
  }



  // on reset Descriptions form
  resetForm() {
    this.noteFormInit();
  }

// play video from clicked bookmark time
  playFromCurrentTime(bookmarkCurrentTime) {
    console.log(bookmarkCurrentTime);
    this.api.currentTime = bookmarkCurrentTime;
    this.api.pause();
  }
  setSelection(type: string) {
    this.selectedButton[type] = true;
  }
  addShape(type: string) {
    this.api.pause();
    this.setSelection(type);
    if (type === 'circle') {
      this.addCircle(this.selectedColorCode);
     } else if (type === 'line') {
      this.addLines(this.selectedColorCode);
     } else if (type === 'rectangle') {
      this.addRectangle(this.selectedColorCode);
    } else if (type === 'text') {
      this.addText(this.selectedColorCode);
    } else if (type === 'arrow') {
      this.addArrow(this.selectedColorCode);
     } else if (type === 'angle') {
      this.addAngle(this.selectedColorCode);
    }
  }
  addText(colorCode) {
    this.lineService.lines(colorCode, true);
    const text = this.textService.text(colorCode);
    this.shapes.push(text);
  }
  addRectangle(colorCode) {
    this.lineService.lines(colorCode, true);
    const rectangle = this.rectangleService.rectangles(colorCode);
    this.shapes.push(rectangle);
  }
  addCircle(colorCode) {
    this.lineService.lines(colorCode, true);
    const circles = this.circleService.circles(colorCode);
    this.shapes.push(circles);
  }
  addAngle(colorCode) {
    this.lineService.lines(colorCode, true);
    const angles = this.angleService.angles(colorCode);
    this.shapes.push(angles);
   }
  addLines(colorCode) {
    this.isCLearAllEnabled = true;
    const lines = this.lineService.lines(colorCode, false);
    this.shapes.push(lines);
  }
  addArrow(colorCode) {
    this.lineService.lines(colorCode, true);
    const arrow = this.arrowService.arrows(colorCode);
    this.shapes.push(arrow);
  }
  clearAllPath() {
    this.lineService.lines('', true);
    d3.selectAll('path.line').remove();
  }
  selectColor(colorCode: any) {
    this.selectedColorCode = colorCode;
  }

  openSettingsTab() {
    this.api.pause();
    this.isSettingTabOpen = true;
    this.isVideoFullScreen = true;
   }
  openVideoOnFullScreen() {
    this.isVideoFullScreen = !this.isVideoFullScreen;
  }
  createAnnotateData(annotateData) {
    return this._FB.group({
      frame_time : [annotateData ? annotateData.frame_time: ''],
      label: [annotateData?annotateData.label:''],
      image_url: [annotateData ? annotateData.image_url: ''],
      description: [annotateData ? annotateData.description: ''],
      duration: [annotateData ? annotateData.duration:'']
    })
  }
  onSaveAnnotateImg() {
    this.lineService.lines('', true);
    // this.api.play();
    const scrubBar = document.getElementsByClassName('scrubBar') as HTMLCollection;
    const scrubBarAriaValue = scrubBar[0].getAttribute('aria-valuenow').replace('%', '');
    console.log('scrubBarAriaValue', scrubBarAriaValue)
    html2canvas(this.screen.nativeElement, {backgroundColor: null, scale: 1, removeContainer: false, allowTaint : true}).then(canvas => {
      this.videoAnnotateImg = canvas.toDataURL('image/png');
      // tslint:disable-next-line:max-line-length
      this.videoAnnotateData.push({frame_time: this.api.currentTime, image_url: this.videoAnnotateImg, convertedTIme: scrubBarAriaValue});
        let block = this.videoAnnotateImg.split(";");
        let contentType = block[0].split(":")[1];
        let realData = block[1].split(",")[1];
        const sliceSize = 512;
        const base64ToBlob = this.base64ToBlob(realData,contentType, sliceSize)
        this.speedImgUploadService.uploadImage(base64ToBlob, 'annotations').subscribe((res)=>{
            if (res){
              let annotateData = {
                frame_time: this.api.currentTime,
                image_url: res.data.file_url,
                duration: scrubBarAriaValue,
              }
              this.AnnotationsArray.push(this.createAnnotateData(annotateData));
              console.log('note form', this.noteForm.value);

            }
        });
    });
    d3.selectAll('g').remove();
    d3.selectAll('path').remove();
    this.isSettingTabOpen = false;
  }
  // on submit descriptions form
  onSubmit() {
    this.api.play();
    let route = '/speed/video/annotations';
    console.log(this.noteForm.value);
    this.videoServices.post(route,this.noteForm.value).subscribe((res)=>{
      console.table(res)
      alert('data added');
      this.getSpeedData();
    })
    if (this.noteForm.valid) {
      this.isToggleNotesForm = true;
      this.videoTimeSlots.push(this.noteForm.value);
      this.noteFormInit();
    } else {
      this.isToggleNotesForm = false;
    }
    this.getSpeedData();
    this.noteFormInit();
  }
  closeSettingsTab() {
    this.lineService.lines('', true);
    this.isVideoFullScreen = false;
    this.api.pause();
    this.isSettingTabOpen = false;
  }

  getSpeedData(){
    const route = '/speed/15'
    this.videoServices.get(route).subscribe((res)=>{
      if (res) {
        this.videoData = res.data;
        console.log(this.videoData);
        this.noteForm['controls'].video_id.patchValue(this.videoData.video.id);
      }
    })
  }


  showAnnotateImg(frame_time, api_currentTime){
    if ( frame_time >= api_currentTime && frame_time <= api_currentTime + 0.30 ) {
        return true;
    }
  }
  skipButtons(skipTime, skipType){
    this.api.pause();
    this.isVideoFullScreen = true;
    if (skipType === 'forward') {
     this.api.currentTime += skipTime;
      console.log(this.api.currentTime)
    }
    if (skipType === 'backward') {
      this.api.currentTime -= skipTime;
      console.log(this.api.currentTime)
    }
  }
  onSubmitCommentForm() {
    const commentCapturedImg = this.getSnapShotAtCurrentTime();
    // tslint:disable-next-line:max-line-length
    let block = commentCapturedImg.split(";");
    let contentType = block[0].split(":")[1];
    let realData = block[1].split(",")[1];
    const sliceSize = 512;
    const base64ToBlob = this.base64ToBlob(realData,contentType, sliceSize);
    this.speedImgUploadService.uploadImage(base64ToBlob, 'annotations').subscribe((res)=> {
      if (res) {
        console.log(res.data.file_url);
        let annotateData = {
          label: this.commentForm['controls'].label.value,
          description: this.commentForm['controls'].description.value,
          frame_time: this.api.currentTime,
          image_url: res.data.file_url,
        }
        this.AnnotationsArray.push(this.createAnnotateData(annotateData));
        this.onSubmit();
        this.getSpeedData();
        this.commentFormInit()
      }
    });
  }
  // get snapshot of video on click on bookmark button
  getSnapShotAtCurrentTime() {
    const canvasElement = document.createElement('CANVAS') as HTMLCanvasElement;
    const videos = this.videoElement.nativeElement;
    const context = canvasElement.getContext('2d');
    // tslint:disable-next-line:one-variable-per-declaration
    let w: number, h: number, ratio: number;
    ratio = videos.videoWidth / videos.videoHeight;
    w = videos.videoWidth - 10;
    h = w / ratio;
    canvasElement.width = w;
    canvasElement.height = h;
    context.fillRect(0, 0, w, h);
    context.drawImage(videos, 0, 0, w, h);
    const link = document.createElement('a');
    this.snapshotName = this.snapshotName !== '' ?  this.snapshotName : 'snapshot';
    this.userImageType = this.imageTypes.indexOf(this.userImageType) >= 0 ? this.userImageType.toUpperCase() : 'PNG';
    link.setAttribute('download', this.snapshotName + '.' + this.userImageType);
    this.capturedImgSrc = canvasElement.toDataURL("image/jpeg",0.7);
    return this.capturedImgSrc;
  }
  // convert base64 to blob
  base64ToBlob(b64Data: any, contentType: any, sliceSize: number) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    let byteCharacters = atob(b64Data);
    let byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      let slice = byteCharacters.slice(offset, offset + sliceSize);

      let byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      let byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }
    return  new Blob(byteArrays, {type: contentType});
  }
  // convert current time into mm:ss
  timeConverter(currentTIme) {
    const tempMinute = Math.floor(currentTIme / 60);
    const minute = (tempMinute >= 10) ? tempMinute : '0' + tempMinute;
    const tempSecond = Math.floor( currentTIme % 60);
    const second = (tempSecond >= 10) ? tempSecond : '0' + tempSecond;
    return  minute + ':' + second;
  }
}
